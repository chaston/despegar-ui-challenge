# Despegar UiChallenge - Gastón Ambrogi
El desafío consta de crear un entorno de trabajo en el cual se deberán desarrollar dos componentes. 
La meta consiste en desarrollar el clúster correspondiente al resultado de Hoteles y el clúster de resultados de Vuelos. 
El diseño de ambos componentes en sus respectivas resoluciones están dentro del archivo adjunto.
Requerimientos:

    Utilizar de ser necesario una grilla de 12 columnas a elección.
    Los componentes deben ser mobile first y estar desarrollados en Sass, PostCSS o Less.
    Re-utilizar la mayor cantidad de elementos y componentes posibles entre ambos clústers.
    Utilizar Angular, Vue o React para renderizar los componentes en una vista.

Se valorará:

    Utilización de NPM o Yarn para gestionar las dependencias.
    Utilización de metodología BEM, prolijidad del código y comentarios descriptivos.
    Utilizar ExpressJS u otro framework/librería de Node para servir los componentes.
    Se apreciaría implementar el json adjunto simulando ser un servicio de resultados para renderizar 2 clústers de hoteles con sus diferentes casuisticas.

-----

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.4.

## Install dependencies
1. `npm install`
1. `npm install -g @angular/cli`

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### The exposed urls of the app are:
* `http://localhost:4200/hotels`
* `http://localhost:4200/flights`

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Serve Static
Run `npm run serve-statics` for serve previous builded artifacts. Navigate to `http://localhost:3000/`. 

### The exposed urls as static are:
* `http://localhost:3000/hotels`
* `http://localhost:3000/flights`

-----
### General comments
* The styles organization was based on ITCSS architecture, placed in `./src/assets`.
* ITCSS organization:
  * Settings
    * variables: Global shared variables of the app.
  * Generic
    * bootstrap-reboot and font-awesome styles.
  * Base
    * Global styles for some HTML elements of the app.
  * Objects: OOCSS
    * img-responsive: Fluid images for responsive purposes.
  * Components: uses BEM for class naming.
    * badge: Badge is used on flights and hotels(_features_) 
    * buttons: Button styles.
    * footer-promo: Component used in flights.
    * forms: Styles for radio buttons and defines it composition.
    * fare: Fare component used in hotels and flights.
    * hotel: Hotel component
    * flight: Flight component.
