El desafío consta de crear un entorno de trabajo en el cual se deberán desarrollar dos componentes. 
La meta consiste en desarrollar el clúster correspondiente al resultado de Hoteles y el clúster de resultados de Vuelos. 
El diseño de ambos componentes en sus respectivas resoluciones están dentro del archivo adjunto.
Requerimientos:

    Utilizar de ser necesario una grilla de 12 columnas a elección.
    Los componentes deben ser mobile first y estar desarrollados en Sass, PostCSS o Less.
    Re-utilizar la mayor cantidad de elementos y componentes posibles entre ambos clústers.
    Utilizar Angular, Vue o React para renderizar los componentes en una vista.

Se valorará:

    Utilización de NPM o Yarn para gestionar las dependencias.
    Utilización de metodología BEM, prolijidad del código y comentarios descriptivos.
    Utilizar ExpressJS u otro framework/librería de Node para servir los componentes.
    Se apreciaría implementar el json adjunto simulando ser un servicio de resultados para renderizar 2 clústers de hoteles con sus diferentes casuisticas.