import { IHotelsResults, IHotel } from './hotel.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class HotelsService {

  constructor() { }

  getHotels$(): Observable<IHotelsResults> {
    return of(<IHotelsResults>{
      results: [
        {
          'name': 'Hotel Fasano',
          'rating': '8.5',
          'recommended': true,
          'stars': 5,
          'discount': 30,
          'price': '2.119',
          'amenities': [
            'wifi',
            'coffee',
            'laundry'
          ],
          'features': [
            'Cancele gratis',
            'Incluye un día de Auto'
          ]
        },
        {
          'name': 'Hotel Faena',
          'rating': '6.5',
          'recommended': false,
          'stars': 3,
          'discount': false,
          'price': '1.819',
          'amenities': [
            'coffee'
          ],
          'features': [
            'Cancele gratis'
          ]
        }
      ]
    });
  }
}
