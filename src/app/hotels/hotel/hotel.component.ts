import { Component, Input } from '@angular/core';
import { IHotel } from '../hotel.model';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html'
})
export class HotelComponent {
  @Input() hotel: IHotel;
  constructor() { }
}
