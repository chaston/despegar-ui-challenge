export interface IHotel {
  name: string;
  rating: string;
  recommended: boolean;
  stars: number;
  discount: number;
  price: string;
  distance?: string;
  amenities: string[];
  features: string[];
}
export interface IHotelsResults {
  results: IHotel[];
}
