import { HotelsService } from './hotels.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotelsComponent } from './hotels/hotels.component';
import { HotelComponent } from './hotel/hotel.component';
import { HotelsRoutingModule } from './hotels-routing.module';
import { NumberToIterablePipe } from './number-to-iterable.pipe';
import { ApplyPriceDiscountPipe } from './apply-price-discount.pipe';


@NgModule({
  imports: [
    CommonModule,
    HotelsRoutingModule
  ],
  providers: [ HotelsService ],
  declarations: [HotelsComponent, HotelComponent, NumberToIterablePipe, ApplyPriceDiscountPipe]
})
export class HotelsModule { }
