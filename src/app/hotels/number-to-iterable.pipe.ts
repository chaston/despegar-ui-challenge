import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberToIterable'
})
export class NumberToIterablePipe implements PipeTransform {

  transform(value: number): any {
    return Array(value).fill(true);
  }

}
