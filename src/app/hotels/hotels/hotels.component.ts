import { IHotelsResults } from './../hotel.model';
import { Observable } from 'rxjs';
import { HotelsService } from './../hotels.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html'
})
export class HotelsComponent implements OnInit {
  hotels$: Observable<IHotelsResults>;
  constructor(private hotels: HotelsService) { }

  ngOnInit() {
    this.hotels$ = this.hotels.getHotels$();
  }

}
