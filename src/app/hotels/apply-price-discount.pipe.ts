import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'applyPriceDiscount'
})
export class ApplyPriceDiscountPipe implements PipeTransform {

  transform(price: any, discount?: any): any {
    price = +price.replace('.', '');
    if (discount) {
      return ( price * (1 - ((discount) / 100))).toFixed(2);
    }
    return price;
  }

}
