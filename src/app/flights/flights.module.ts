import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightsComponent } from './flights/flights.component';
import { FlightComponent } from './flight/flight.component';
import { FlightsRoutingModule } from './/flights-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FlightsRoutingModule
  ],
  declarations: [FlightsComponent, FlightComponent]
})
export class FlightsModule { }
